# Knot DNS Smoke Tests

This repository contains tools to deduplicate DNS queries from a PCAP captures.

The deduplicated packets can be later used to smoke test an authoritative DNS
server.

## Dependencies

To compile the utilities, following libraries are required:

- Nettle (for hashes computation)
- libmsgpack (for MessagePack format support)
- libpcap (for PCAP format support)

## File format

PCAP is used only as an input (and optionally an output) format. In all other
cases, the MessagePack format is used. No internal structure is used with
MessagePack. The content is just a sequence of binary objects.

Advantage of MessagePack is that multiple files can be simply concantenated.
That is not possible with PCAP due to a presence of a header.

## Packet Processing Workflow

This is how the tool set is supposed to work:

```
    capture.pcap
         |
         v
    +---------+
    | extract |
    +---------+
         |
         v
 cleaned-packets.mp
         |
         v
  +-------------+
  | deduplicate | <------------  hashes.mp
  +-------------+                   ^
         |                          |
         v                   +-------------+
  unique-packets.mp -------> | dump-hashes |
         |                   +-------------+
         v
    +---------+
    | mp2pcap |
    +---------+
         |
         v
 unique-packets.pcap

```

### capture

```
./capture < capture.pcap > packets.mp
```

The utility reads a PCAP from stdin. The program expects RAW capture without
L2 data. The L3 and L4 layers are stripped and only a DNS message is retained.
If the L4 is TCP, the message length is removed. Also the DNS transaction ID
is set to zeroes.

Only DNS query messages are preserved. The responses, fragmented packet, or
incomplete captures are ignored.

The individual messages are writen to stdout in MessagePack format.

### deduplicate

```
./deduplicate [hashes.mp] < packets.mp > unique-packets.mp
```

The utility reads DNS messages in MessagePack format from stdin, computes
SHA-1 hashes from the messages, and uses the hashes to determine unique
messages.

Unique messages are copied to stdout.

Optionally, the utility can be seeded with already known hashes.

### dump-hashes

```
./dump-hashes < packets.mp > hashes.mp
```

The utility reads DNS messages in MessagePack format from stdin, computes
their SHA-1 hashes, and writes these hashes in a MessagePack format to stdout.

### mp2pcap

```
./mp2pcap < packets.mp > packets.pcap
```

The utility converts stored DNS messages in MessagePack format to PCAP format.
The output PCAP format uses `DLT_USER0` link-type. No L2-4 information is
saved, just a raw DNS message.

## Author

Jan Včelák

## Licence

Not decided yet.
