#include <assert.h>
#include <stdio.h>

#include "hash.h"
#include "hat-trie/hat-trie.h"
#include "pack.h"

static bool load_hash(const uint8_t *data, size_t size, void *ctx)
{
	hattrie_t *hashes = ctx;

	if (size != HASH_SIZE) {
		fprintf(stderr, "invalid hash size, got %zu, expected %d\n", size, HASH_SIZE);
		return false;
	}

	value_t *record = hattrie_get(hashes, (char *)data, size);
	assert(record);
	*record = 1;

	return true;
}

static int load_hashes(hattrie_t *hashes, const char *filename)
{
	FILE *in = fopen(filename, "r");
	if (!in) {
		perror("load hashes, fopen");
		return 1;
	}

	unpacker_t *unpacker = unpacker_new(in);
	assert(unpacker);

	if (!unpacker_unpack_all(unpacker, load_hash, hashes)) {
		fprintf(stderr, "failed to load hashes\n");
		fclose(in);
		unpacker_free(unpacker);
		return 1;
	}

	fclose(in);
	unpacker_free(unpacker);

	return 0;
}

struct dedup_ctx {
	hattrie_t *hashes;
	packer_t *packer;
	size_t unique_count;
};

static bool is_unique(hattrie_t *hashes, const uint8_t *data, size_t size)
{
	uint8_t hash[HASH_SIZE] = { 0 };
	hash_packet(hash, data, size);

	value_t *val = hattrie_get(hashes, (char *)hash, sizeof(hash));
	if (*val == 0) {
		*val = 1;
		return true;
	} else {
		return false;
	}
}

static bool deduplicate_one(const uint8_t *data, size_t size, void *_ctx)
{
	struct dedup_ctx *ctx = _ctx;

	if (is_unique(ctx->hashes, data, size)) {
		packer_pack(ctx->packer, data, size);
		ctx->unique_count += 1;
	}

	return true;
}

static int deduplicate(hattrie_t *hashes, FILE *in, FILE *out)
{
	unpacker_t *unpacker = unpacker_new(in);
	packer_t *packer = packer_new(out);

	assert(unpacker);
	assert(packer);

	struct dedup_ctx ctx = { .hashes = hashes, .packer = packer };
	int result = unpacker_unpack_all(unpacker, deduplicate_one, &ctx) ? 0 : 1;

	fprintf(stderr, "[summary] %zu unique packets dumped\n", ctx.unique_count);

	packer_free(packer);
	unpacker_free(unpacker);

	return result;
}

int main(int argc, char *argv[])
{
	if (argc > 2) {
		fprintf(stderr, "usage: %s [<dumped-hashes>] < input\n", argv[0]);
		return 1;
	}

	int result = -1;

	hattrie_t *hashes = hattrie_create();

	if (argc == 2 && (result = load_hashes(hashes, argv[1])) != 0) {
		goto done;
	}

	result = deduplicate(hashes, stdin, stdout);

done:
	hattrie_free(hashes);

	return result;
}
