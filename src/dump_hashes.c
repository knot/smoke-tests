#include <assert.h>
#include <stdio.h>

#include "hash.h"
#include "pack.h"

static bool dump_hash(const uint8_t *data, size_t size, void *ctx)
{
	packer_t *packer = ctx;

	uint8_t hash[HASH_SIZE] = { 0 };
	hash_packet(hash, data, size);

	packer_pack(packer, hash, sizeof(hash));

	return true;
}

int main(int argc, char *argv[])
{
	if (argc < 1) {
		fprintf(stderr, "usage: %s < packets.mp\n", argv[0]);
		return 1;
	}

	int result = 1;

	unpacker_t *unpacker = unpacker_new(stdin);
	packer_t *packer = packer_new(stdout);

	assert(unpacker);
	assert(packer);

	result = unpacker_unpack_all(unpacker, dump_hash, packer) ? 0 : 1;
	if (result != 0) {
		fprintf(stderr, "[error] failed to dump hashes\n");
	}

	unpacker_free(unpacker);
	packer_free(packer);

	return result;
}
