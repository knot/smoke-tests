#include <assert.h>
#include <pcap.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>

#include "pack.h"

struct binary {
	uint8_t *data;
	size_t size;
};

typedef struct binary binary_t;

static void binary_skip(binary_t *binary, size_t add)
{
	assert(binary->size >= add);

	binary->data += add;
	binary->size -= add;
}

enum {
	PARSE_OK = 0,
	PARSE_SKIP,
	PARSE_MALFORMED_PACKET,
	PARSE_FRAGMENTED_PACKET,
	PARSE_UNSUPPORTED_PROTOCOL,
};

static int parse_l3(binary_t *wire, uint8_t *l4_proto)
{
	if (wire->size < 1) {
		return PARSE_MALFORMED_PACKET;
	}

	uint8_t version = wire->data[0] >> 4;
	if (version == 4 && wire->size >= sizeof(struct iphdr)) {
		const struct iphdr *ip = (void *)wire->data;
		if (wire->size != htons(ip->tot_len)) {
			return PARSE_FRAGMENTED_PACKET;
		}
		size_t hdr_size = 4 * ip->ihl;
		if (wire->size < hdr_size) {
			return PARSE_MALFORMED_PACKET;
		}
		*l4_proto = ip->protocol;
		binary_skip(wire, hdr_size);
		return PARSE_OK;
	} if (version == 6 && wire->size >= sizeof(struct ip6_hdr)) {
		const struct ip6_hdr *ip6 = (void *)wire->data;
		*l4_proto = ip6->ip6_nxt;
		binary_skip(wire, sizeof(*ip6));
		return PARSE_OK;
	} else {
		return PARSE_UNSUPPORTED_PROTOCOL;
	}
}

static int parse_l4(binary_t *wire, uint8_t proto)
{
	if (proto == IPPROTO_UDP) {
		size_t hdr_size = sizeof(struct udphdr);
		if (wire->size < hdr_size) {
			return PARSE_MALFORMED_PACKET;
		}
		binary_skip(wire, hdr_size);
		return PARSE_OK;
	} else if (proto == IPPROTO_TCP) {
		if (wire->size < sizeof(struct tcphdr)) {
			return PARSE_MALFORMED_PACKET;
		}
		const struct tcphdr *hdr = (void *)wire->data;
		size_t hdr_len = hdr->doff * 4;
		if (wire->size < hdr_len) {
			return PARSE_MALFORMED_PACKET;
		} else if (wire->size == hdr_len) {
			return PARSE_SKIP;
		}

		binary_skip(wire, hdr_len);
		assert(wire->size > 0);

		uint16_t size = ntohs(*(uint16_t *)wire->data);
		if (size != wire->size - 2) {
			return PARSE_MALFORMED_PACKET;
		}

		binary_skip(wire, 2);
		return PARSE_OK;
	} else {
		return PARSE_UNSUPPORTED_PROTOCOL;
	}
}

static int seek_dns(const binary_t *packet, binary_t *dns_ptr)
{
	if (packet->size < 1) {
		return PARSE_MALFORMED_PACKET;
	}

	binary_t dns = *packet;

	uint8_t l4_proto;
	int r = parse_l3(&dns, &l4_proto);
	if (r != PARSE_OK) {
		return r;
	}

	r = parse_l4(&dns, l4_proto);
	if (r != PARSE_OK) {
		return r;
	}

	*dns_ptr = dns;
	return PARSE_OK;
}

static const char *parse_strerror(int code)
{
	switch (code) {
	case PARSE_OK:
		return "success";
	case PARSE_MALFORMED_PACKET:
		return "malformed-packet";
	case PARSE_FRAGMENTED_PACKET:
		return "fragmented-packet";
	case PARSE_UNSUPPORTED_PROTOCOL:
		return "unsupported-protocol";
	default:
		return "unknown-error";
	}
}

struct __attribute__((packed)) dns_hdr {
	uint16_t id;
	uint16_t flags;
	uint16_t qdcount;
	uint16_t ancount;
	uint16_t nscount;
	uint16_t arcount;
};

static int check_if_query(const binary_t *dns)
{
	if (dns->size < sizeof(struct dns_hdr)) {
		return PARSE_MALFORMED_PACKET;
	}

	struct dns_hdr *hdr = (void *)dns->data;
	assert(sizeof(*hdr) == 12);

	// check for Query Response
	bool qr = ntohs(hdr->flags) >> 15;

	return qr ? PARSE_SKIP : PARSE_OK;
}

static int dump_dns(packer_t *packer, const binary_t *dns)
{
	// clear transaction ID
	uint8_t tmp[UINT16_MAX + 1];
	assert(dns->size <= sizeof(tmp));
	memcpy(tmp, dns->data, dns->size);
	memset(tmp, 0, 2);

	packer_pack(packer, tmp, dns->size);

	return PARSE_OK;
}

static void log(size_t packet, const char *msg)
{
	fprintf(stderr, "%zu %s\n", packet, msg);
}

int main(int argc, char *argv[])
{
	if (argc != 1) {
		fprintf(stderr, "usage: %s < pcap > dump 2> errors\n", argv[0]);
		return 1;
	}

	char error[PCAP_ERRBUF_SIZE] = "";
	pcap_t *cap = pcap_fopen_offline(stdin, error);
	if (!cap) {
		perror("pcap_fopen_offline");
		fprintf(stderr, "%s\n", error);
		return 1;
	}

	int dlink = pcap_datalink(cap);
	assert(dlink == DLT_RAW);

	packer_t *packer = packer_new(stdout);
	assert(packer);

	size_t i = 0;
	struct pcap_pkthdr header;
	const uint8_t *data = pcap_next(cap, &header);
	for (/* nop */; data != NULL; i++, data = pcap_next(cap, &header)) {
		if (header.caplen != header.len) {
			log(i, "incomplete-capture");
			continue;
		}

		binary_t packet = { .data = (uint8_t *)data, .size = header.caplen };
		binary_t dns = { 0 };

		int r;

		(r = seek_dns(&packet, &dns), r == PARSE_OK) &&
		(r = check_if_query(&dns), r == PARSE_OK) &&
		(r = dump_dns(packer, &dns));

		if (r == PARSE_OK || r == PARSE_SKIP) {
			continue;
		} else {
			log(i, parse_strerror(r));
		}
	}

	pcap_close(cap);
	packer_free(packer);

	return 0;
}
