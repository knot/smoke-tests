#include <nettle/sha1.h>

#include "hash.h"

void hash_packet(uint8_t *hash, const uint8_t *buffer, size_t size)
{
	struct sha1_ctx ctx;
	nettle_sha1_init(&ctx);
	nettle_sha1_update(&ctx, size, buffer);
	nettle_sha1_digest(&ctx, HASH_SIZE, hash);
}
