#pragma once

#include <stdint.h>
#include <stddef.h>
#include <nettle/sha1.h>

#define HASH_SIZE SHA1_DIGEST_SIZE

void hash_packet(uint8_t *hash, const uint8_t *data, size_t size);
