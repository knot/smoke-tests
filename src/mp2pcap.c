#include <assert.h>
#include <stdio.h>
#include <pcap/pcap.h>

#include "pack.h"

static bool convert_packet(const uint8_t *data, size_t size, void *ctx)
{
	pcap_dumper_t *dumper = ctx;

	struct pcap_pkthdr header = { .caplen = size, .len = size };
	pcap_dump((void *)dumper, &header, data);

	return true;
}

int main(int argc, char *argv[])
{
	if (argc != 1) {
		fprintf(stderr, "usage: %s < input.mp > output.pcap\n", argv[0]);
		return 1;
	}

	unpacker_t *unpacker = unpacker_new(stdin);
	assert(unpacker);

	pcap_t *dump = pcap_open_dead(DLT_USER0, UINT16_MAX + 1);
	pcap_dumper_t *dumper = pcap_dump_fopen(dump, stdout);
	assert(dump && dumper);

	int result = unpacker_unpack_all(unpacker, convert_packet, dumper) ? 0 : 1;
	if (result != 0) {
		fprintf(stderr, "[error] failed to convert all packets\n");
	}

	pcap_dump_close(dumper);
	pcap_close(dump);
	unpacker_free(unpacker);

	return result;
}
