#include <assert.h>
#include <msgpack.h>

#include "pack.h"

#define READ_BLOCK 1024

struct packer {
	FILE *out;
	msgpack_packer mp;
};

struct unpacker {
	FILE *src;
	msgpack_unpacker mup;
};

static int packer_write(void *ctx, const char *buf, size_t len)
{
	packer_t *packer = ctx;
	return fwrite(buf, 1, len, packer->out);
}

packer_t *packer_new(FILE *output)
{
	packer_t *result = calloc(1, sizeof(*result));
	if (!result) {
		return NULL;
	}

	result->out = output;
	msgpack_packer_init(&result->mp, result, packer_write);

	return result;
}

void packer_free(packer_t *packer)
{
	if (!packer) {
		return;
	}

	free(packer);
}

void packer_pack(packer_t *packer, const uint8_t *data, size_t size)
{
	msgpack_pack_raw(&packer->mp, size);
	msgpack_pack_raw_body(&packer->mp, data, size);
}

unpacker_t *unpacker_new(FILE *src)
{
	unpacker_t *result = calloc(1, sizeof(*result));
	if (!result) {
		return NULL;
	}

	result->src = src;
	msgpack_unpacker_init(&result->mup, MSGPACK_UNPACKER_INIT_BUFFER_SIZE);

	return result;
}

void unpacker_free(unpacker_t *unpacker)
{
	if (!unpacker) {
		return;
	}

	msgpack_unpacker_flush_zone(&unpacker->mup);
	msgpack_unpacker_destroy(&unpacker->mup);

	free(unpacker);
}

bool unpacker_unpack_all(unpacker_t *unpacker, unpack_cb handler, void *handler_ctx)
{
	assert(unpacker);
	assert(handler);

	while (!feof(unpacker->src)) {

		// feed buffer

		msgpack_unpacker_reserve_buffer(&unpacker->mup, READ_BLOCK);
		size_t read = fread(msgpack_unpacker_buffer(&unpacker->mup), 1, READ_BLOCK, unpacker->src);
		if (read == 0) {
			break;
		}
		msgpack_unpacker_buffer_consumed(&unpacker->mup, read);

		// unpack content

		msgpack_unpacked unpacked = { 0 };
		msgpack_unpacked_init(&unpacked);
		while (msgpack_unpacker_next(&unpacker->mup, &unpacked)) {
			if (unpacked.data.type != MSGPACK_OBJECT_RAW) {
				return false;
			}
			msgpack_object_raw *raw = &unpacked.data.via.raw;
			bool success = handler((uint8_t *)raw->ptr, raw->size, handler_ctx);
			msgpack_unpacked_destroy(&unpacked);

			if (!success) {
				return false;
			}
		}
	}

	return true;
}
