#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

struct packer;
typedef struct packer packer_t;

packer_t *packer_new(FILE *output);

void packer_free(packer_t *packer);

struct unpacker;
typedef struct unpacker unpacker_t;

typedef bool (*unpack_cb)(const uint8_t *data, size_t size, void *ctx);

unpacker_t *unpacker_new(FILE *input);

void packer_pack(packer_t *packer, const uint8_t *data, size_t size);

void unpacker_free(unpacker_t *unpacker);

bool unpacker_unpack_all(unpacker_t *unpacker, unpack_cb handler, void *handler_ctx);
